
public class IceCream {
  public String flavorIceCream;
  public int numScoops;
  public String typeIceCream;

//to initialize the object
 public IceCream(String flavorIceCream, int numScoops, String typeIceCream){
 this.flavorIceCream = flavorIceCream;
 this.numScoops = numScoops;
 this.typeIceCream = typeIceCream;
 }

//an instance method that checks if the flavourIceCream is a Pistachio
 public boolean bestSeller() {
  if (flavorIceCream.equals("Pistachio")) {
   return true;
  } else {
   return false;
  }
 }
}